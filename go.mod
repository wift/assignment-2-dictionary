module gitlab.se.ifmo.ru/wift/assignment-2-dictionary

go 1.18

require (
	github.com/brianvoe/gofakeit/v6 v6.23.2
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
