package dict

import (
    "os/exec"
	"testing"
	"bytes"
	"strings"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const ShellToUse = "bash"

func Shellout(command string) (string, string, error) {
    var stdout bytes.Buffer
    var stderr bytes.Buffer
    cmd := exec.Command(ShellToUse, "-c", command)
    cmd.Stdout = &stdout
    cmd.Stderr = &stderr
    err := cmd.Run()
    return stdout.String(), stderr.String(), err
}

func TestDict(t *testing.T) {
	testCases := []struct{
		name string
		key string
		isErr bool
		expected string 
	} {
		{
			name: "word exist",
			key: "first key",
			isErr: false,
			expected: "first value",
		},
		{
			name: "too long key",
			key: strings.Repeat("a", 256),
			isErr: true,
			expected: "key string too long",
		},
		{
			name: "word not exist",
			key: "ff",
			isErr: true,
			expected: "key not found",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			out, errout, err := Shellout("echo -n \"" + tc.key + "\" | ./main")

			if tc.isErr {
				require.Error(t, err)
				assert.Equal(t, tc.expected, errout, "wrong error")
				assert.Equal(t, "", out, "not empty stdout")
			} else {
				require.NoError(t, err)
				assert.Equal(t, tc.expected, out, "wrong out")
				assert.Equal(t, "", errout, "empty stderr was expected")
			}
		})
	}
}