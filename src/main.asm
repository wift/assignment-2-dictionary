%include "./src/lib.inc"
%include "./src/dict.inc"
%include "./src/words.inc"

%define BUFFER_SIZE 255

section .rodata
err_too_long: db "key string too long", 0
err_not_found db "key not found", 0

section .text
global _start
_start:
    sub rsp, BUFFER_SIZE

    mov rdi, rsp
    mov rsi, BUFFER_SIZE
    call read_string
    test rdx, rdx
    jz .err_too_long
    push rdx
    
    mov rdi, rax
    mov rsi, head
    call find_word

    test rax, rax
    jz .err_not_found
    pop rdx
    lea rdi, [rax + rdx + 8]
    call print_string
    mov rdi, 0
    add rsp, BUFFER_SIZE
    jmp exit

    .err_too_long:
        mov rdi, err_too_long
        jmp .show_err
    .err_not_found:
        mov rdi, err_not_found
    .show_err:
        call print_err_string
        mov rdi, 1
        add rsp, BUFFER_SIZE
        jmp exit

