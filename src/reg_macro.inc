
%macro push_regs 1-*
    %rep %0
        push %1
        %rotate 1
    %endrep
%endmacro

%macro pop_regs 1-*
    %rep %0
        %rotate -1
        pop %1
    %endrep
%endmacro