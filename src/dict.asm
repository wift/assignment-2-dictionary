%include "./src/lib.inc"
%include "./src/reg_macro.inc"
global find_word

section .text

; Проходит по всему словарю в поисках подходящего ключа. 
; Если подходящее вхождение найдено, возвращает адрес начала вхождения в словарь (не значения),
; иначе возвращает 0.
; rdi Указатель на нуль-терминированную строку.
; rsi Указатель на начало словаря.
find_word:
    push_regs r12, r13
    mov r12, rdi
    mov r13, rsi

    .loop:
        mov rdi, r12
        lea rsi, [r13 + 8]
        call string_equals
        test rax, rax
        jnz .found
        mov r13, [r13] ;!
        test r13, r13
        jnz .loop
    
    .not_found:
        xor rax, rax
        jmp .exit
    .found:
        mov rax, r13
    .exit:
        pop_regs r12, r13
        ret
