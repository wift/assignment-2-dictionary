SRC=./src
SRCS=$(wildcard $(SRC)/*.asm)
OBJ=./obj
OBJS = $(patsubst $(SRC)/%.asm,$(OBJ)/%.o,$(SRCS))
DEP=./dep
DEPS=$(patsubst $(SRC)/%.asm,$(DEP)/%.d,$(SRCS))

ASM=nasm
ASMFLAGS=-f elf64
LD=ld

main: deps $(OBJS)
	$(LD) -o main $(OBJS)

$(OBJ)/%.o: $(SRC)/%.asm
	mkdir -p $(OBJ)
	$(ASM) $(ASMFLAGS) -o $@ $<

ifeq ("$(wildcard $(DEP)/*.d)", $(DEPS))
	include $(DEPS)
endif

.PHONY: deps clean rebuild test

deps:
	@echo "building dependencies.."
	mkdir -p $(DEP)
	for x in $(patsubst $(SRC)/%.asm,%,$(SRCS)); do\
		nasm -f elf64  -M -MF $(DEP)/$$x.d $(SRC)/$$x.asm -MQ $(OBJ)/$$x.o;\
	done

rebuild: clean deps main

clean:
	@echo "cleaning..."
	rm -rf $(OBJ) $(DEP)

test: main
	@go test






